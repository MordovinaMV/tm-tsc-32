package ru.tsc.mordovina.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.mordovina.tm.enumerated.Status;
import ru.tsc.mordovina.tm.model.Project;

public interface IProjectRepository extends IOwnerRepository<Project> {

    boolean existsByName(@NotNull String userId, @NotNull String name);

    @NotNull
    Project findByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Project startById(@NotNull String userId, @NotNull String id);

    @NotNull
    Project startByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    Project startByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Project finishById(@NotNull String userId, @NotNull String id);

    @NotNull
    Project finishByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    Project finishByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Project changeStatusById(@NotNull String userId, @NotNull String id, @NotNull Status status);

    @NotNull
    Project changeStatusByIndex(@NotNull String userId, @NotNull Integer index, @NotNull Status status);

    @NotNull
    Project changeStatusByName(@NotNull String userId, @NotNull String name, @NotNull Status status);

}
