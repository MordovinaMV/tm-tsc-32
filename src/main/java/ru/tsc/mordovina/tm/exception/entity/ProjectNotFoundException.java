package ru.tsc.mordovina.tm.exception.entity;

import ru.tsc.mordovina.tm.exception.AbstractException;

public class ProjectNotFoundException extends AbstractException {

    public ProjectNotFoundException() {
        super("Error. Project not found.");
    }

}
