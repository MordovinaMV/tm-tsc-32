package ru.tsc.mordovina.tm.exception.entity;

import ru.tsc.mordovina.tm.exception.AbstractException;

public class UserNotFoundException extends AbstractException {

    public UserNotFoundException() {
        super("Error. User not found.");
    }

}
